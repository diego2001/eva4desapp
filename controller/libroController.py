import mysql.connector

from model.conexionbdd import *
from model.libro import *

def obtener():
    try:
        connection=get_connection()
        cursor=connection.cursor()
        query = "SELECT * FROM libros"
        cursor.execute(query)
        tabla = cursor.fetchall()
        for columna in tabla:
            print("ID libro:",columna[0],"\nFecha de prestamo:", columna[1],"\nFecha de entrega:", columna[2],"\nEstado:", columna[3],"\nSituación:", columna[4],"\nTítulo:", columna[5],"\nID usuario:", columna[6])
            print()
    except(Exception, mysql.connector.Error) as error:
        exceptionE="Error",error
        return exceptionE
def listarLibros():
    try:
        connection = get_connection()
        cursor = connection.cursor()
        query = "SELECT * FROM libros"
        cursor.execute(query)
        tuplas = cursor.fetchall()
        listaLibros=[]
        for row in tuplas:
            libro = Libro()
            libro.setFecha_prestamo(row[1])
            libro.setFecha_entrega(row[2])
            libro.setEstado(row[3])
            libro.setSituacion(row[4])
            libro.setTitulo(row[5])
            libro.setId_usuario(row[6])
            listaLibros.append(libro)
            close_connection(connection)
            return listaLibros

    except(Exception, mysql.connector.Error) as error:
        exceptionE="Error en la conexión a la BDD", error
        return exceptionE

def agregarLibros(libro):
    try:
        connection = get_connection()
        cursor = connection.cursor()
        query = """INSERT INTO libros (fecha_prestamo,fecha_entrega,estado,situacion,titulo,id_usuario) VALUES 
        (%s,%s,%s,%s,%s,%s)"""
        cursor.execute(query, (libro.getFecha_prestamo(), libro.getFecha_entrega(), libro.getEstado(),
                               libro.getSituacion(), libro.getTitulo(), libro.getId_usuario()))
        connection.commit()
        close_connection(connection)
        mensaje = "Libro agregado con éxito!"
        return mensaje
    except(Exception, mysql.connector.Error) as error:
        exceptionE = "Error en la conexión BDD", error
        return exceptionE