from model.conexionbdd import *
from model.usuario import *

def listarUsuario():
    try:
        connection = get_connection()
        cursor = connection.cursor()
        query = "SELECT * FROM usuario"
        cursor.execute(query)
        tuplas = cursor.fetchall()
        listaUsuario=[]
        for row in tuplas:
            usuario = Usuario()
            usuario.setNombre(row[1])
            usuario.setApellido(row[2])
            usuario.setRut(row[3])
            usuario.setTelefono(row[4])
            usuario.setDirección(row[5])
            listaUsuario.append(usuario)
            close_connection(connection)
            return listaUsuario

    except(Exception, mysql.connector.Error) as error:
        exceptionE="Error en la conexión a la BDD", error
        return exceptionE

def agregarUsuario(usuario):
    try:
        connection = get_connection()
        cursor = connection.cursor()
        query = """INSERT INTO usuario (nombre,apellido,rut,telefono,dirección) VALUES 
        (%s,%s,%s,%s,%s)"""
        cursor.execute(query, (usuario.getNombre(), usuario.getApellido(), usuario.getRut(),
                               usuario.getTelefono(), usuario.getDirección()))
        connection.commit()
        close_connection(connection)
        mensaje = "Libro agregado con éxito!"
        return mensaje
    except(Exception, mysql.connector.Error) as error:
        exceptionE = "Error en la conexión BDD", error
        return exceptionE