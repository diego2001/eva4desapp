import mysql.connector


def get_connection():
    connection = mysql.connector.connect(host="localhost", database="sistema_de_prestamo_de_libros", user="root",
                                         password="")
    return connection


def close_connection(connection):
    if connection:
        connection.close()
