class Usuario:
    def __init__(self, nombre, apellido, rut, telefono, dirección):
        self._nombre = nombre
        self._apellido = apellido
        self._rut = rut
        self._telefono = telefono
        self._dirección = dirección

    def __init__(self):
        self._nombre = ""
        self._apellido = ""
        self._rut = ""
        self._telefono = ""
        self._dirección = ""

    def getNombre(self):
        return self._nombre

    def getApellido(self):
        return self._apellido

    def getRut(self):
        return self._rut

    def getTelefono(self):
        return self._telefono

    def getDirección(self):
        return self._dirección

    def setNombre(self, nombre):
        self._nombre = nombre

    def setApellido(self, apellido):
        self._apellido = apellido

    def setRut(self, rut):
        self._rut = rut

    def setTelefono(self, telefono):
        self._telefono = telefono

    def setDirección(self, dirección):
        self._dirección = dirección

    def toString2(self):
        return f"""Nombre: {self._nombre}\nApellido: {self._apellido}\nRut: {self._rut}
\nTeléfono: {self._telefono}\nDirección: {self._dirección}"""
