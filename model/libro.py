from model.conexionbdd import *

class Libro:
    def __init__(self, fecha_prestamo, fecha_entrega, estado, situacion, titulo, id_usuario):
        self._fecha_prestamo = fecha_prestamo
        self._fecha_entrega = fecha_entrega
        self._estado = estado
        self._situacion = situacion
        self._titulo = titulo
        self._id_usuario = id_usuario

    def __init__(self):
        self._fecha_prestamo = ""
        self._fecha_entrega = ""
        self._estado = ""
        self._situacion = ""
        self._titulo = ""
        self._id_usuario = ""

    def getFecha_prestamo(self):
        return self._fecha_prestamo

    def getFecha_entrega(self):
        return self._fecha_entrega

    def getEstado(self):
        return self._estado

    def getSituacion(self):
        return self._situacion

    def getTitulo(self):
        return self._titulo

    def getId_usuario(self):
        return self._id_usuario

    def setFecha_prestamo(self, fecha_prestamo):
        self._fecha_prestamo = fecha_prestamo

    def setFecha_entrega(self, fecha_entrega):
        self._fecha_entrega = fecha_entrega

    def setEstado(self, estado):
        self._estado = estado

    def setSituacion(self, situacion):
        self._situacion = situacion

    def setTitulo(self, titulo):
        self._titulo = titulo

    def setId_usuario(self, id_usuario):
        self._id_usuario = id_usuario